[Code](/code/)

# Nui Code Reference
*For how to get started, see the [`Learn`](/learn) section.*

<!-- articlecontent -->
Welcome to the developer reference for all things `Nui`!

If you're new to `Nui` check out the [Quick Start](/learn/quick-start) guide.

## Getting Started

If you're looking to jump right in, we'd suggest taking a look at the [Quick Start](/learn/quick-start) guide to become familiar. `Nui` is incredibly easy, but it's a bit different than some of the other packages out there.

Most of the dependencies are taken care of with **`nui-simple`** including `Babel` and `React.js`, so you really don't need to add anything else.

```shell
$ yarn add nui-simple
```
*(You can also checkout the [`example-app`](/learn/quick-start) for a good boost!)*

A typical project structure is very simple, and looks like the following:

* /src/actions/
* /src/stores/
* /src/pages/
* /src/views/

Everything inside the `/src/` directory is built using Babel's [`env` preset](https://npmjs.org/packages/babel-preset-env) by default. If you have a *`.babelrc`* in the root of your project, it will be leveraged for building your project.

Fork the [`quick-start`](/repo/quick-start) repo or [`example-app`](/repo/example-app) repos to get started quickly.

If you want to contribute to the project and help `Nui` grow, take a peek at the [Contributor's Guide](/learn/contribute) on how to get started. It's much easier than you may think :)

<!-- endarticlecontent -->

<!-- seealso -->
## Developer Reference
* [Action Reference](/code/nui/action)
* [Page Reference](/code/nui/page)
* [Store Reference](/code/nui/store)
* [View Reference](/code/nui/view)


* [Document.jsx](/code/document)
* [NotFound.jsx](/code/not-found)
* [Project Structure](/learn/project-structure)

**API**
* [Actions Interface](/code/nui/interfaces/action)
* [Store Interface](/code/nui/interfaces/store)
* [Component Interface](/code/nui/interfaces/component)



* **Packages**
	* [`markdown-pages`](/code/pkgs/markdown-pages)

## Contributor Reference

* **Modules**
	* [`nui` (Core)](/code/modules/nui)
	* [`nui-simple`](/code/modules/simple)
	* [`nui-build`](/code/modules/build)
	* [`nui-build-watch`](/code/modules/build-watch)
	* [`nui-builder-babel`](/code/modules/builder-babel)
	* [`nui-platform-browser`](/code/modules/platform-browser)
	* [`nui-platform-node`](/code/modules/platform-node)
	* [`nui-utils`](/code/modules/utils)
	* [`nui-logger`](/code/modules/logger)

* [Contributor's Guide](/learn/contribute)


<!-- endseealso -->

<!-- topics -->
## Related Topic Guides

* [Creating Your First Component](/learn/first-component)
* [Creating Your First Page](/code/first-page)
* [How Dependencies Work in Nui](/code/how-dependencies-work)
* [Todo App Example](/code/example-app)
* [Stylesheets and Styled Components](/code/styling-tips)
* [Project Structures](/learn/project-structure)

<!-- endtopics -->
