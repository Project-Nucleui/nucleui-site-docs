[Code](/code/) > [document.jsx](/code/document)

# NotFound Route (404 Page)

<!-- articlecontent -->

The `NotFound` page is a required component that is served any time NeueRouter is unable to find a route for the URL specified by the user.

## Syntax

*See also [Component Syntax](/code/nui/view#syntax).*

```javascript
import React from 'react';

export const Routes = 'NotFound';

export default nui => ({ location }) => (
	<main>
		<h1>404 Page Not Found</h1>
		<hr />
		<p>The page you&apos;re looking for does not exist: {location.pathname}</p>
	</main>
);
```

## Component Props

###### `location`
> *`object`* Instance of Location containing the URL info of the requested page.

## Examples

### Adding Stylesheets
Stylesheets can be added the same way as any traditional HTML element. Simply add your *`<link />`* tag in the *`<head />`* and you're golden.

```javascript
import React from 'react';

export default (NeueUI) => ({ App, AppScript }) => (
	<html>
		<head>
			<title>NeueLogic</title>
			<link rel="stylesheet" href="/css/reset.css" />
			<link rel="stylesheet" href="/css/global.css" />
		</head>
		<body>
			<App />
			<AppScript />
		</body>
	</html>
);
```

### Example Using `styled-components`
A highly recommended and versatile way of styling is to use [`styled-components`](https://www.styled-components.com/), and rendering your stylesheets is incredibly easy by using the [*`ServerStyleSheet`*](https://www.styled-components.com/docs/advanced#server-side-rendering) export provided by [`styled-components`](https://www.styled-components.com/).

```javascript
import React from 'react';
import { ServerStyleSheet } from 'styled-components'

export default (NeueUI) => ({ App, AppScript }) => {
	const sheet = new ServerStyleSheet();
	const main = sheet.collectStyles(<App />);
	const styleTags = sheet.getStyleElement();

	return (
		<html>
			<head>
				<title>NeueLogic</title>
				{styleTags}
			</head>
			<body>
				{main}
				<AppScript />
			</body>
		</html>
	);
}
```

<!-- endarticlecontent -->

<!-- seealso -->
##### Document

* [**Syntax**](/code/document#syntax)
* [**Component Props**](/code/document#component-props)
	* [**props.App**](/code/document#app)
	* [**props.AppScript**](/code/document#appscript)
* [**Examples**](/code/document#examples)
	* [**Adding Stylesheets**](/code/document#adding-stylesheets)
	* [**Example Using `styled-components`**](/code/document#example-using-styled-components)

**Related:**
* [**NotFound Route**](/code/not-found)
* [**Styling Tips**](/code/styling-tips)
* [**ResourceRef**](/code/nui/resource-ref)
* [**Router**](/code/nui/router)
* [**Types**](/code/nui/types)

<!-- endseealso -->

<!-- topics -->
* [Quick Start Guide](/learn/quick-start)
* [Project Structure](/learn/project-structure)
* [Views, Stores, Actions: What Are They?](/learn/resources/what-are-they)
* [How Dependencies Work in Nui](/learn/how-dependencies-work)
<!-- endtopics -->
